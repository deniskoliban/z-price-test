import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StateService } from './state.service';
import { environment } from '../../../environments/environment';
import { finalize, map } from 'rxjs/operators';
import { User, UserCreateOptions } from '../../models/user-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(
    private stateService: StateService,
    private http: HttpClient
) {}

  getUsersList() {
    this.http.get(environment.api + '/users')
      .pipe(
        map((users: UserCreateOptions[]) => {
          return users.map((user) => User.create(user))
        })
      )
      .subscribe((users) => {
        this.stateService.users$.next(users);
      })
  }

  getUser(id: number): Observable<UserCreateOptions> {
    return this.http.get<UserCreateOptions>(environment.api + '/users/' + id)
  }

  redactUser(id: number, user: UserCreateOptions): Observable<User> {
    return this.http.put(environment.api + '/users/' + id, user)
      .pipe(
        map((res: UserCreateOptions) => User.create(res)),
        finalize(() => this.getUsersList())
      )
  }

  addUser(user: UserCreateOptions): Observable<User> {
    return this.http.post(environment.api + '/users', user)
      .pipe(
        map((res: UserCreateOptions) => User.create(res)),
        finalize(() => this.getUsersList())
      )
  }

  deleteUser(id: number): Observable<User> {
    return this.http.delete(environment.api + '/users/' + id)
      .pipe(
        map((res: UserCreateOptions) => User.create(res)),
        finalize(() => this.getUsersList())
      )
  }
}
