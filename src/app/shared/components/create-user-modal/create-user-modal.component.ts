import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-user-modal',
  templateUrl: './create-user-modal.component.html',
  styleUrls: ['./create-user-modal.component.sass']
})
export class CreateUserModalComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  formControls = {
    firstName: new FormControl(null, Validators.required),
    surname: new FormControl(null, Validators.required),
    patronymic: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email]),
    phoneNumber: new FormControl(null, [
      Validators.required,
      Validators.pattern(/^380[\s.-]?\d{2}[\s.-]?\d{3}[\s.-]?\d{2}[\s.-]?\d{2}$/)
    ]),
    address: new FormControl(null, Validators.required),
  }
  phoneNumberSubscription: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private httpService: HttpService
  ) {
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group(this.formControls);
    let prevVal = '';
    this.phoneNumberSubscription = this.formControls.phoneNumber.valueChanges
      .subscribe((val) => {
        if (val) {
          const lastCharacter: string = val?.length ? val.slice(-1) : '';
          if (lastCharacter?.match(/[\d-.]/)) {
            prevVal = val;
          } else {
            this.formControls.phoneNumber.setValue(prevVal);
          }
        }
      })
  }

  submit() {
    if (this.formGroup.valid) {
      const userData = this.formGroup.value
      userData.phoneNumber = userData.phoneNumber.replace(/[\s.-]/g, '')
      this.httpService.addUser(this.formGroup.value)
        .subscribe(() => this.activeModal.close())
    }
  }

  public ngOnDestroy(): void {
    this.phoneNumberSubscription.unsubscribe();
  }
}
