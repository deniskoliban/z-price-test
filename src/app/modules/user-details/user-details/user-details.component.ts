import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HttpService } from '../../../shared/services/http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.sass']
})
export class UserDetailsComponent implements OnInit, OnDestroy {
  id: number;
  formGroup: FormGroup;
  formControls = {
    firstName: new FormControl(null, Validators.required),
    surname: new FormControl(null, Validators.required),
    patronymic: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email]),
    phoneNumber: new FormControl(null, [
      Validators.required,
      Validators.pattern(/^380[\s.-]?\d{2}[\s.-]?\d{3}[\s.-]?\d{2}[\s.-]?\d{2}$/)
    ]),
    address: new FormControl(null, Validators.required),
  }
  phoneNumberSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group(this.formControls);
    this.id = this.activatedRoute.snapshot.params.id;
    this.httpService.getUser(this.id)
      .subscribe(({ firstName, surname, patronymic, email, phoneNumber, address, }) => {
        this.formGroup.setValue({
          firstName,
          surname,
          patronymic,
          email,
          phoneNumber,
          address,
        });
      })
    let prevVal = '';
    this.phoneNumberSubscription = this.formControls.phoneNumber.valueChanges
      .subscribe((val) => {
        if (val) {
          const lastCharacter: string = val?.length ? val.slice(-1) : '';
          if (lastCharacter?.match(/[\d-.]/)) {
            prevVal = val;
          } else {
            this.formControls.phoneNumber.setValue(prevVal);
          }
        }
      })
  }

  submit() {
    if (this.formGroup.valid && this.id) {
      const userData = this.formGroup.value
      userData.phoneNumber.replace(/[\s.-]/, '')
      this.httpService.redactUser(this.id, userData)
        .subscribe((res) => {
          console.log(res);
          this.toMainPage();
        })
    }
  }

  toMainPage() {
    this.router.navigate(['/main']);
  }

  public ngOnDestroy(): void {
  }

}
