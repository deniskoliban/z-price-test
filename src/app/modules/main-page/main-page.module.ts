import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
  ]
})
export class MainPageModule { }
