import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserDetailsRoutingModule } from './user-details-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [UserDetailsComponent],
    imports: [
        CommonModule,
        UserDetailsRoutingModule,
        ReactiveFormsModule
    ]
})
export class UserDetailsModule { }
