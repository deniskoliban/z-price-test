import { Component, OnInit } from '@angular/core';
import { CreateUserModalComponent } from '../../../shared/components/create-user-modal/create-user-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StateService } from '../../../shared/services/state.service';
import { HttpService } from '../../../shared/services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})
export class MainPageComponent implements OnInit {

  constructor(
    public stateService: StateService,
    private modalService: NgbModal,
    private httpService: HttpService,
    private router: Router
  ) {}

  ngOnInit(): void {

  }

  public openCreateModal(): void {
    this.modalService.open(CreateUserModalComponent);
  }

  public deleteUser(id: number) {
    this.httpService.deleteUser(id)
      .subscribe()
  }

  public goToDetails(id: number): void {
    this.router.navigate([`/user/user-details/${id}`])
  }
}
