import { Exclude, Expose, plainToClass } from 'class-transformer';

export interface UserCreateOptions {
  id: number;
  firstName: string;
  surname: string;
  patronymic: string;
  email: string;
  phoneNumber: string;
  address: string;
}

@Exclude()
export class User {
  @Expose()
  public id: number;

  @Expose()
  public firstName: string;

  @Expose()
  public surname: string;

  @Expose()
  public patronymic: string;

  @Expose()
  public email: string;

  @Expose()
  public phoneNumber: string;

  @Expose()
  public address: string;

  public static create(data: UserCreateOptions): User {
    return plainToClass(User, data);
  }

  get fullName() {
    return this.firstName + ' ' + this.surname + ' ' + this.patronymic;
  }

}
